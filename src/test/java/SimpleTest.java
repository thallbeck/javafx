import BaseClasses.LoginType;
import BaseClasses.WebPage;
import StudentPortalPages.myHomePage;
import StudentPortalPages.myMathCenterPage;
import Utils.General;
import Utils.PingSession;
import org.testng.annotations.Test;

/*
 * Created by timothy.hallbeck on 8/29/2016.
 */

public class SimpleTest {

    @Test
    public void justMathPage() {
        myMathCenterPage mathPage = new myMathCenterPage(LoginType.PROD_CPOINDEXTER);
        mathPage.ExercisePage(true);
    }

    @Test
    public void ping() {
        PingSession ping = new PingSession();
        String result;

//        result = ping.Get("web4.qa.wgu.edu/cpip/assm_30_ver1.asp", LoginType.LANE1_SPOLTUS);
//        General.Debug(result);

//        result = ping.Get("web5.qa.wgu.edu/ewb_rpt/masterlaunch.asp", LoginType.LANE1_SPOLTUS);
//        General.Debug(result);

//        result = ping.Get("web3.qa.wgu.edu/wguResources/employeeResources.asp", LoginType.LANE1_SPOLTUS);
//        General.Debug(result);

        result = ping.Get("web3.qa.wgu.edu/ResourceTools/Default.aspx", LoginType.LANE2_CPOINDEXTER);
        General.Debug(result);

    }

    @Test
    public void ExerciseMyHomePageQA() throws Exception {
        myHomePage myPage = null;

        try {
            myPage = new myHomePage(null, LoginType.LANE1_NORMAL, WebPage.Browser.Firefox);
            myPage.ExercisePage(true);
        } catch (Exception e) {
            General.Debug(e.getMessage());
            throw new Exception(e);
        } finally {
            if(myPage != null)
                myPage.quit();
        }
    }
}
