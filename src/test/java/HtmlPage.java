
/*
 * Created by timothy.hallbeck on 9/6/2016.
 */

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Side;
import javafx.scene.chart.PieChart;
import org.testng.annotations.Test;

import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.io.File;
import java.io.FileOutputStream;

public class HtmlPage {
    @Test
    public void aTest() {
        HTMLDocument doc = new HTMLDocument();
        HTMLEditorKit kit = new HTMLEditorKit();

        File f = new File("greeting.html");

        try {
            String string = "<img src='file:///C:/Users/timothy.hallbeck/Desktop/pie-fruits.PNG'/>";
            kit.insertHTML(doc, doc.getLength(), "Hello 1<br>", 0, 0, null);
            kit.insertHTML(doc, doc.getLength(), "Hello 2<br>", 0, 0, null);
            kit.insertHTML(doc, doc.getLength(), string, 0, 0, null);

            FileOutputStream fos = new FileOutputStream(f);
            kit.write(fos, doc, 0, doc.getLength());
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        new PieChart.Data("Person", 13),
                        new PieChart.Data("Cohorts", 25),
                        new PieChart.Data("Degree Plan", 10),
                        new PieChart.Data("Mentors", 22),
                        new PieChart.Data("Courses", 30));
        final PieChart chart = new PieChart(pieChartData);

        chart.setTitle("Machine count");
        chart.setLegendSide(Side.BOTTOM);


    }

}

