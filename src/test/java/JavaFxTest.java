
/*
 * Created by timothy.hallbeck on 9/8/2016.
 */

import ServiceClasses.ServiceProcessor;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.testng.annotations.Test;

public class JavaFxTest extends Application {

    private ComboBox<String> UserCombo = null;
    private ComboBox<String> ServiceCombo = null;
    private ComboBox<String> AuthCombo = null;
    private ComboBox<String> ActionCombo = null;
    private Button GoButton = null;
    private Button ClearButton = null;
    private TextArea OutputWindow = null;

    private double WindowHeight = 550;
    private double WindowWidth = 1200;

    @Test
    public void launchFX() {
        launch("oh", "really");
    }

    private void InitDataItems() {
        InitOutputArea();
        InitUserCombo();
        InitServiceCombo();
        InitAuthTypeCombo();
        InitActionCombo();
        InitGoButton();
        InitClearButton();
    }

    @Override
    public void start(Stage stage) {
        InitDataItems();

        stage.setTitle("Services");
        stage.setWidth(WindowWidth);
        stage.setHeight(WindowHeight);

        VBox root = new VBox();
        GridPane grid1 = new GridPane();
        grid1.setVgap(4);
        grid1.setHgap(10);
        grid1.setPadding(new Insets(5, 5, 5, 5));

//        grid1.setGridLinesVisible(true);
        grid1.add(new Label("  Service:"), 0, 0);
        grid1.add(ServiceCombo, 1, 0);
        grid1.add(new Label("  User:"), 2, 0);
        grid1.add(UserCombo, 3, 0);
        grid1.add(new Label("  Auth type:"), 4, 0);
        grid1.add(AuthCombo, 5, 0);
        grid1.add(new Label("  Action:"), 6, 0);
        grid1.add(ActionCombo, 7, 0);
        grid1.add(new Label(" ---> "), 8, 0);
        grid1.add(GoButton, 9, 0);

        GridPane grid2 = new GridPane();
        grid2.add(new Label("  "), 0, 2);
        grid2.add(ClearButton, 0, 3);
        grid2.add(new Label("  "), 0, 4);
        grid2.add(OutputWindow, 0, 5);

        root.getChildren().addAll(grid1, grid2);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    private void InitUserCombo() {
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        ServiceProcessor.GetLogins()
                );

        UserCombo = new ComboBox<>(options);
        UserCombo.setValue("Lane 2 CPoindexter");

        if (UserCombo.getItems().size() > 5)
            UserCombo.setVisibleRowCount(5);

        UserCombo.setOnAction(event -> {
            String name = event.getEventType().getName();
            if (name.equals("ACTION")) {
                System.out.println("User selection changed to " + UserCombo.getValue());
            }
        });
    }

    private void InitServiceCombo() {
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        ServiceProcessor.GetServiceNames()
                );
        ServiceCombo = new ComboBox<>(options);
        ServiceCombo.setValue("Person Service");

        if (ServiceCombo.getItems().size() > 10)
            ServiceCombo.setVisibleRowCount(10);

        ServiceCombo.setOnAction(event -> {
            String name = event.getEventType().getName();
            if (name.equals("ACTION"))
                System.out.println("Service selection changed to " + ServiceCombo.getValue());
        });
    }

    private void InitAuthTypeCombo() {
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        ServiceProcessor.GetAuthTypes()
                );
        AuthCombo = new ComboBox<>(options);
        AuthCombo.setValue("Ping");

        if (AuthCombo.getItems().size() > 5)
            AuthCombo.setVisibleRowCount(5);

        AuthCombo.setOnAction(e -> {
                    if (e.getEventType().getName().equals("ACTION"))
                        System.out.println("AuthType selection changed to " + AuthCombo.getValue());
                }
        );

    }

    private void InitActionCombo() {
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        ServiceProcessor.GetActions()
                );
        ActionCombo = new ComboBox<>(options);
        ActionCombo.setValue("Return an auth token");

        ActionCombo.setOnAction(e -> {
                    if (e.getEventType().getName().equals("ACTION")) {
                        System.out.println("ActionCombo selection changed to " + ActionCombo.getValue());
                    }
                }
        );
    }

    private void InitOutputArea() {
        OutputWindow = new TextArea();
        OutputWindow.setWrapText(false);
        OutputWindow.setPrefSize(2 * WindowWidth, 2 * WindowHeight);
    }

    private void InitGoButton() {
        GoButton = new Button("  Go  ");
        GoButton.setOnAction(event -> {
                    String name = event.getEventType().getName();
                    if (name.equals("ACTION")) {
//                        General.SetOutputWindow(OutputWindow);
                        new ServiceProcessor().Process(ServiceCombo.getValue(), UserCombo.getValue(), AuthCombo.getValue(), ActionCombo.getValue(), OutputWindow);
                    }
                    return;
                }
        );
    }

    private void InitClearButton() {
        ClearButton = new Button("  Clear Output Window ");
        ClearButton.setOnAction(event -> {
                    String name = event.getEventType().getName();
                    if (name.equals("ACTION")) {
                        OutputWindow.setText("");
                    }
                }
        );
    }

}

