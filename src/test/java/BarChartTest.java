
/*
 * Created by timothy.hallbeck on 9/29/2016.
 */

import Utils.General;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A chart that fills in the area between a line of data points and the axes.
 * Good for comparing accumulated totals over time.
 *
 * @related charts/line/LineChart
 * @related charts/scatter/ScatterChart
 * @see javafx.scene.chart.Chart
 * @see javafx.scene.chart.Axis
 * @see javafx.scene.chart.NumberAxis
 */
public class BarChartTest extends Application {
    private static final int MAX_DATA_POINTS = 50;

    private Series series;
    private int xSeriesData = 0;
    private ConcurrentLinkedQueue<Number> dataQ = new ConcurrentLinkedQueue<Number>();
    private ExecutorService executor;
    private AddToQueue addToQueue;
    private NumberAxis xAxis;
    private Button button;

    private void init(Stage primaryStage) {
        xAxis = new NumberAxis(0, MAX_DATA_POINTS, MAX_DATA_POINTS / 10);
        xAxis.setForceZeroInRange(false);
        xAxis.setAutoRanging(false);

        button = new Button(" \"Ping\" ");
        button.setOnAction(event -> {
                    String name = event.getEventType().getName();
                    if (name.equals("ACTION")) {
                        Toolkit.getDefaultToolkit().beep();
                    }
                    return;
                }
        );
        NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRanging(true);

        //-- Chart
        final AreaChart<Number, Number> sc = new AreaChart<Number, Number>(xAxis, yAxis) {
            // Override to remove symbols on each data point
            @Override
            protected void dataItemAdded(Series<Number, Number> series, int itemIndex, Data<Number, Number> item) {
            }
        };
        sc.setAnimated(false);
        sc.setId("liveAreaChart");
        sc.setTitle("Animated Area Chart");

        //-- Chart Series
        series = new AreaChart.Series<Number, Number>();
        series.setName("Area Chart Series");
        sc.getData().add(series);

        VBox root = new VBox();
        root.getChildren().addAll(sc, button);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        init(primaryStage);
        primaryStage.show();

        //-- Prepare Executor Services
        executor = Executors.newCachedThreadPool();
        addToQueue = new AddToQueue();
        executor.execute(addToQueue);
        //-- Prepare Timeline
        prepareTimeline();
    }

    @Override
    public void stop() throws Exception {
        timer.stop();
        executor.shutdownNow();
    }

    public static void main(String[] args) {
        launch(args);
    }

    // Do whatever call out to get data here, its in its own thread
    // Ensure that silent mode is on
    private class AddToQueue implements Runnable {
        public void run() {
            try {
                // add a item of random data to queue
                if (executor.isShutdown())
                    return;
                dataQ.add(Math.random());
                Thread.sleep(500);
                executor.execute(this);
            } catch (InterruptedException ex) {
                General.Debug(ex.getMessage());
            }
        }
    }

    static AnimationTimer timer;
    //-- Timeline gets called in the JavaFX Main thread
    private void prepareTimeline() {
        // Every frame to take any data from queue and add to chart
        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                addDataToSeries();
            }
        };
        timer.start();
    }

    private void addDataToSeries() {
        for (int i = 0; i <= 1; i++) { //-- add 20 numbers to the plot+
            if (dataQ.isEmpty())
                break;
            series.getData().add(new AreaChart.Data(xSeriesData++, dataQ.remove()));
        }
        // remove points to keep us at no more than MAX_DATA_POINTS
        if (series.getData().size() > MAX_DATA_POINTS) {
            series.getData().remove(0, series.getData().size() - MAX_DATA_POINTS);
        }
        // update
        xAxis.setLowerBound(xSeriesData - MAX_DATA_POINTS);
        xAxis.setUpperBound(xSeriesData - 1);
    }
}